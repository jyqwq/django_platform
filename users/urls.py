from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^getUserRoutes', views.get_user_routes, name='get_user_routes'),
    url(r'^getUserInfo', views.get_user_msgV2, name='get_user_msgV2'),
    url(r'^getUserMsg', views.get_user_msg, name='get_user_msg'),
    url(r'^updateToken', views.update_token, name='update_token'),
    url(r'^logout', views.logout, name='logout'),
    url(r'^loginV2', views.loginV2, name='login_v2'),
    url(r'^login', views.login, name='login'),
]
