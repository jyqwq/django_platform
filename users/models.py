from django.db import models
from systemManage import models as sysModels


# 管理账号表
class AdminUser(models.Model):
    admin_user = models.CharField(max_length=20, unique=True)  # 账号
    password = models.CharField(max_length=255)  # 密码
    permission = models.ForeignKey(sysModels.MenuPermission, on_delete=models.CASCADE)  # 权限等级


# 用户表
class CommonUser(models.Model):
    account = models.CharField(max_length=20, unique=True)  # 账号
    admin_group = models.ForeignKey(AdminUser, on_delete=models.CASCADE)  # 所属管理组
    password = models.CharField(max_length=255)  # 密码
    nickname = models.CharField(max_length=8)  # 昵称
    icon = models.CharField(max_length=254)  # 头像
