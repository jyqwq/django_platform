from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
import json
from auths import models as auths_models
from utils.token import create_token, check_token, create_refresh_token, refresh_token
from utils import utils


def index(request):
    return HttpResponse("Hello, world. You're at the users index.")


def login(request):
    try:
        if request.method == 'POST' and request.body:
            login_msg = json.loads(request.body)
            if login_msg.get("account") and login_msg.get("password"):
                # 查找用户信息
                msg = auths_models.AuthUser.objects.filter(account=login_msg["account"]).values()
                if len(msg):
                    admin_user = list(msg)[0]
                    # 检查密码
                    if admin_user["password"] == login_msg["password"]:
                        # 生成token 存入session
                        request.session["token"] = create_token(admin_user["id"], request.META.get("REMOTE_ADDR"))
                        return get_user(admin_user)
                    return JsonResponse({"state": 500, "message": "密码错误", "data": {}}, safe=False)
                else:
                    return JsonResponse({"state": 500, "message": "用户不存在", "data": {}}, safe=False)
            else:
                return JsonResponse({"state": 500, "message": "请输入账号或密码", "data": {}}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": {}}, safe=False)
    except Exception as ex:
        print("login error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": {}}, safe=False)


def get_user_msg(request):
    try:
        if request.method == 'GET':
            token = request.session.get("token")
            data = check_token(token)
            if data is None:
                request.session.flush()
                return JsonResponse({"state": 300, "message": "token过期", "data": {}}, safe=False)
            # 获取用户信息
            admin_user = model_to_dict(auths_models.AuthUser.objects.get(id=data["user_id"]))
            return get_user(admin_user)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": {}}, safe=False)
    except Exception as ex:
        print("get_user_msg error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": {}}, safe=False)


def logout(request):
    try:
        if request.method == 'GET':
            request.session.flush()
            return JsonResponse({"state": 200, "message": "退出成功", "data": {}}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": {}}, safe=False)
    except Exception as ex:
        print("logout error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": {}}, safe=False)


def loginV2(request):
    try:
        if request.method == 'POST' and request.body:
            login_msg = json.loads(request.body)
            if login_msg.get("account") and login_msg.get("password"):
                # 查找用户信息
                msg = auths_models.AuthUser.objects.filter(account=login_msg["account"]).values()
                if len(msg):
                    admin_user = list(msg)[0]
                    # 检查密码
                    if admin_user["password"] == login_msg["password"]:
                        token = create_token(admin_user["id"], request.META.get("REMOTE_ADDR"))
                        r_token = create_refresh_token(admin_user["id"], request.META.get("REMOTE_ADDR"))
                        request.session["token"] = token
                        return JsonResponse({"state": 200, "message": "登录成功",
                                             "data": {"token": token, "refreshToken": r_token}}, safe=False)
                    return JsonResponse({"state": 500, "message": "密码错误", "data": {}}, safe=False)
                else:
                    return JsonResponse({"state": 500, "message": "用户不存在", "data": {}}, safe=False)
            else:
                return JsonResponse({"state": 500, "message": "请输入账号或密码", "data": {}}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": {}}, safe=False)
    except Exception as ex:
        print("loginV2 error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": {}}, safe=False)


def update_token(request):
    try:
        if request.method == 'POST' and request.body:
            login_msg = json.loads(request.body)
            if login_msg.get("refreshToken"):
                token = refresh_token(login_msg["refreshToken"])
                if token is None:
                    return JsonResponse({"state": 200, "message": "refreshToken过期", "data": ""}, safe=False)
                return JsonResponse({"state": 200, "message": "更新成功", "data": token}, safe=False)
            else:
                return JsonResponse({"state": 500, "message": "refreshToken不存在", "data": {}}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": {}}, safe=False)
    except Exception as ex:
        print("loginV2 error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": {}}, safe=False)


def get_user_msgV2(request):
    try:
        if request.method == 'GET':
            data = check_token(request.headers["Authorization"])
            # 获取用户信息
            admin_user = model_to_dict(auths_models.AuthUser.objects.get(id=data["user_id"]))
            role = model_to_dict(auths_models.AuthRole.objects.get(id=admin_user["roleId"]))
            user = {
                "userId": admin_user["id"],
                "userName": admin_user["name"],
                "userRole": role["permission"],
            }
            return JsonResponse({"state": 200, "message": "请求成功", "data": user}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": {}}, safe=False)
    except Exception as ex:
        print("get_user_msgV2 error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": {}}, safe=False)


def get_user_routes(request):
    try:
        if request.method == 'POST' and request.body:
            body = json.loads(request.body)
            if body.get("userId"):
                # 获取用户信息
                admin_user = model_to_dict(auths_models.AuthUser.objects.get(id=body["userId"]))
                return get_routes(admin_user)
            else:
                return JsonResponse({"state": 500, "message": "参数错误", "data": {}}, safe=False)

        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": {}}, safe=False)
    except Exception as ex:
        print("get_user_routes error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": {}}, safe=False)


def get_user(admin_user):
    role = model_to_dict(auths_models.AuthRole.objects.get(id=admin_user["roleId"]))
    permission = role["permission"]
    user_info = {
        "id": admin_user["id"],
        "account": admin_user["account"],
        "name": admin_user["name"],
        "roleId": admin_user["roleId"],
        "role": {
            "name": role["name"],
            "permission": permission
        }
    }
    routes = []
    parent_list = []
    if permission == "admin":
        permission_items = ["*"]
        menu_list = utils.build_menu_tree(False, list(auths_models.AuthMenu.objects.all().values()))
    else:
        permissions = list(auths_models.AuthPermission.objects.filter(roleId=admin_user["roleId"]).values())
        for item in permissions:
            menu = model_to_dict(auths_models.AuthMenu.objects.get(id=item["menuId"]))
            if menu["parentId"] != 0 and menu["parentId"] not in parent_list:
                parent = model_to_dict(auths_models.AuthMenu.objects.get(id=menu["parentId"]))
                parent_list.append(parent["id"])
                routes.append(parent)
            routes.append(menu)
        menu_list = utils.build_menu_tree(False, routes)
        permission_items = utils.get_permission_items(admin_user["roleId"])
    user_info["role"]["permissionItems"] = permission_items
    return JsonResponse({"state": 200, "message": "登录成功",
                         "data": {"userInfo": user_info, "routes": menu_list}}, safe=False)


def get_routes(admin_user):
    role = model_to_dict(auths_models.AuthRole.objects.get(id=admin_user["roleId"]))
    permission = role["permission"]
    routes = []
    parent_list = []
    if permission == "admin":
        menu_list = utils.build_menu_tree(False, list(auths_models.AuthMenu.objects.all().values()))
    else:
        permissions = list(auths_models.AuthPermission.objects.filter(roleId=admin_user["roleId"]).values())
        for item in permissions:
            menu = model_to_dict(auths_models.AuthMenu.objects.get(id=item["menuId"]))
            if menu["parentId"] != 0 and menu["parentId"] not in parent_list:
                parent = model_to_dict(auths_models.AuthMenu.objects.get(id=menu["parentId"]))
                parent_list.append(parent["id"])
                routes.append(parent)
            routes.append(menu)
        menu_list = utils.build_menu_tree(False, routes)
    return JsonResponse({"state": 200, "message": "登录成功", "data": {"home": "dashboard_analysis", "routes": menu_list}}, safe=False)
