# Generated by Django 3.2.4 on 2023-08-08 07:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('systemManage', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdminUser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('admin_user', models.CharField(max_length=20, unique=True)),
                ('password', models.CharField(max_length=255)),
                ('permission', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='systemManage.menupermission')),
            ],
        ),
        migrations.CreateModel(
            name='CommonUser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account', models.CharField(max_length=20, unique=True)),
                ('password', models.CharField(max_length=255)),
                ('nickname', models.CharField(max_length=8)),
                ('icon', models.CharField(max_length=254)),
                ('admin_group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.adminuser')),
            ],
        ),
    ]
