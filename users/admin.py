from django.contrib import admin
from .models import AdminUser, CommonUser

admin.site.register(AdminUser)
admin.site.register(CommonUser)
