from django.http import JsonResponse
from django.utils.deprecation import MiddlewareMixin
from utils.token import check_token

unCheckLogin = ['/users/login', '/users/loginV2', '/users/updateToken']


class CheckLogin(MiddlewareMixin):

    def process_request(self, request):
        if request.path not in unCheckLogin:
            # token = request.session.get("token")
            token = request.headers["Authorization"]
            if token is not None:
                data = check_token(token)
                if data is None or data.get('type') != 'access' or not data.get('user_id'):
                    return JsonResponse({"state": 520, "message": "token失效", "data": {}}, safe=False)
                elif data.get('ip') != request.META['REMOTE_ADDR']:
                    return JsonResponse({"state": 520, "message": "token失效", "data": {}}, safe=False)
            else:
                return JsonResponse({"state": 520, "message": "未登录", "data": {}}, safe=False)

    def process_exception(self, request, exception):
        print('CheckLogin error')
        return JsonResponse({"state": 500, "message": "服务器错误", "data": str(exception)}, safe=False)

    def process_response(self, request, response):
        return response
