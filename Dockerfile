FROM jyqaq/rainbow-django

MAINTAINER "JyQAQ"

COPY . .

RUN pip3 config set global.index-url https://mirrors.aliyun.com/pypi/simple/

RUN pip3 install -r requirements.txt

ENTRYPOINT uwsgi --ini /opt/app/django_platform/uwsgi.ini && nginx -g "daemon off;"