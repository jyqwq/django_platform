from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('menu/getMenu', views.get_menu, name='get_menu'),
    path('menu/addMenu', views.add_menu, name='add_menu'),
    path('menu/delMenu', views.del_menu, name='del_menu'),
    path('menu/upMenu', views.up_menu, name='up_menu'),
    path('menu/getMenuTemplate', views.get_menu_template, name='get_menu_template'),
    path('menu/addMenuTemplate', views.add_menu_template, name='add_menu_template'),
    path('menu/delMenuTemplate', views.del_menu_template, name='del_menu_template'),
    path('menu/upMenuTemplate', views.up_menu_template, name='up_menu_template'),
]
