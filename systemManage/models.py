from django.db import models


# 菜单表
class Menu(models.Model):
    name = models.CharField(max_length=24)  # 菜单名称
    higher = models.IntegerField()  # 上级菜单
    icon = models.CharField(max_length=60)  # 菜单图标
    url = models.CharField(max_length=60, default='/home/index')  # 路由地址
    path = models.CharField(max_length=60)  # 页面路径


# 菜单模型表
class MenuPermission(models.Model):
    menu_name = models.CharField(max_length=24)  # 菜单名称
    permission = models.IntegerField(unique=True)  # 权限等级
    menu_list = models.JSONField()  # 菜单模型数据
