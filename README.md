# django_platform

## 生成requirements.txt文件
`pip freeze > requirements.txt`
## 安装依赖
`pip install -r requriements.txt`

## 生成新应用
`python manage.py startapp appname`

## 更新数据库模型
### 生成迁移文件
`python manage.py makemigrations`
### 执行迁移文件
`python manage.py migrate`

## 生成Django管理员
`python manage.py createsuperuser`

## 项目结构

| 文件名称                    | 文件说明           |
| --------------------------- | ------------------ |
| django_platform             | 项目主目录         |
| django_platform/settings.py | 项目配置           |
| django_platform/urls.py     | 项目主路由         |
| static_all                  | 静态文件           |
| users                       | 用户模块           |
| utils                       | 工具               |
| .gitlab-ci.yml              | gitlab-ci配置文件  |
| django_platform.conf        | nginx配置文件      |
| Dockerfile                  | docker镜像生成文件 |
| manage.py                   | 项目入口           |
| requirements.txt            | 项目依赖           |
| uwsgi.ini                   | uWSGI配置文件      |

## 接口权限

```python
# 需要登录才能访问的接口
LOGIN_REQUIRED_URLS = {
    '/users/getUserMsg',
    '/system/menu/getMenu',
    '/system/menu/addMenu',
    '/system/menu/delMenu',
    '/system/menu/upMenu',
    '/system/menu/getMenuSelect',
}
# 需要1级权限的接口
PERMISSION_1 = {
    '/system/menu/getMenu',
    '/system/menu/addMenu',
    '/system/menu/delMenu',
    '/system/menu/upMenu',
    '/system/menu/getMenuSelect',
}
```



## 接口文档

### 管理员登录

> /users/login/admin

**请求方式：**POST

**请求参数：**

|   字段   |    说明    |  类型  | 备注 | 是否必填 |
| :------: | :--------: | :----: | :--: | :------: |
|  admin   | 管理员账号 | 字符串 |      |    是    |
| password | 管理员密码 | 字符串 |      |    是    |

**返回参数：**

|  字段   |     说明     | 类型 |        备注         |
| :-----: | :----------: | :--: | :-----------------: |
|  state  | 接口返回状态 | int  |   200成功/500异常   |
| message | 接口返回信息 | str  | 登录成功/密码错误等 |
|  data   |              | obj  |                     |

### 普通用户登录

> /users/login/common

**请求方式：**POST

**请求参数：**

账号密码登录

|   字段   |    说明    |  类型  | 备注 | 是否必填 |
| :------: | :--------: | :----: | :--: | :------: |
| account  | 管理员账号 | 字符串 |      |    是    |
| password | 管理员密码 | 字符串 |      |    是    |

**返回参数：**

|  字段   |     说明     | 类型 |        备注         |
| :-----: | :----------: | :--: | :-----------------: |
|  state  | 接口返回状态 | int  |   200成功/500异常   |
| message | 接口返回信息 | str  | 登录成功/密码错误等 |
|  data   |   用户信息   | obj  |                     |

### 普通用户注册

> /users/register

**请求方式：**POST

**请求参数：**

|    字段    |   说明   |  类型  |      备注       | 是否必填 |
| :--------: | :------: | :----: | :-------------: | :------: |
|  account   | 注册账号 | 字符串 |                 |    是    |
|  password  | 注册密码 | 字符串 |                 |    是    |
| admin_user | 管理员ID |  int   | 所属管理，默认1 |    否    |

**返回参数：**

|  字段   |     说明     | 类型 |         备注          |
| :-----: | :----------: | :--: | :-------------------: |
|  state  | 接口返回状态 | int  |    200成功/500异常    |
| message | 接口返回信息 | str  | 注册成功/账号已注册等 |
|  data   |   注册信息   | obj  |    账号初始化信息     |

### 获取用户信息

> /users/getUserMsg

**请求方式：**GET

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |   用户信息   | obj  |  用户名和路由   |

### 退出登录

> /users/logout

**请求方式：**GET

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |              | obj  |                 |

### 获取菜单列表

> /system/menu/getMenu

**请求方式：**GET

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |   列表信息   | obj  |                 |

### 新增菜单

> /system/menu/addMenu

**请求方式：**POST

**请求参数：**

|  字段  |    说明    |  类型  | 备注 | 是否必填 |
| :----: | :--------: | :----: | :--: | :------: |
|  name  |  菜单名称  | 字符串 |      |    是    |
| higher | 上级菜单ID |  int   |      |    是    |
|  path  |  路由地址  | 字符串 |      |    是    |
|  icon  |  图标信息  | 字符串 |      |    否    |

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |   新菜单ID   | int  |                 |

### 删除菜单

> /system/menu/delMenu

**请求方式：**POST

**请求参数：**

| 字段 |  说明  | 类型 | 备注 | 是否必填 |
| :--: | :----: | :--: | :--: | :------: |
|  id  | 菜单ID | int  |      |    是    |

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |  删除菜单ID  | int  |                 |

### 更新菜单

> /system/menu/upMenu

**请求方式：**POST

**请求参数：**

|  字段  |    说明    |  类型  | 备注 | 是否必填 |
| :----: | :--------: | :----: | :--: | :------: |
|   id   |   菜单ID   |  int   |      |    是    |
|  name  |  菜单名称  | 字符串 |      |    否    |
| higher | 上级菜单ID |  int   |      |    否    |
|  path  |  路由地址  | 字符串 |      |    否    |
|  icon  |  图标信息  | 字符串 |      |    否    |

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |  更新菜单ID  | int  |                 |

### 获取菜单模型列表

> /system/menu/getMenuTemplate

**请求方式：**GET

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |   列表信息   | obj  |                 |

### 新增菜单模型

> /system/menu/addMenuTemplate

**请求方式：**POST

**请求参数：**

|    字段    |   说明   |   类型   | 备注 | 是否必填 |
| :--------: | :------: | :------: | :--: | :------: |
|    name    | 菜单名称 |  字符串  |      |    是    |
| permission | 权限等级 |   int    | 唯一 |    是    |
|    list    | 菜单模型 | json数组 |      |    是    |

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |   新菜单ID   | int  |                 |

### 删除菜单模型

> /system/menu/delMenuTemplate

**请求方式：**POST

**请求参数：**

| 字段 |  说明  | 类型 | 备注 | 是否必填 |
| :--: | :----: | :--: | :--: | :------: |
|  id  | 菜单ID | int  |      |    是    |

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |  删除菜单ID  | int  |                 |

### 更新菜单模型

> /system/menu/upMenuTemplate

**请求方式：**POST

**请求参数：**

|    字段    |   说明   |   类型   | 备注 | 是否必填 |
| :--------: | :------: | :------: | :--: | :------: |
|     id     |  菜单ID  |   int    |      |    是    |
|    name    | 菜单名称 |  字符串  |      |    否    |
| permission | 权限等级 |   int    | 唯一 |    否    |
|    list    | 菜单模型 | json数组 |      |    否    |

**返回参数：**

|  字段   |     说明     | 类型 |      备注       |
| :-----: | :----------: | :--: | :-------------: |
|  state  | 接口返回状态 | int  | 200成功/500异常 |
| message | 接口返回信息 | str  |                 |
|  data   |  更新菜单ID  | int  |                 |
