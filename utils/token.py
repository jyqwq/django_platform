import jwt

SECRET_KEY = "rainbowInPaper"


def create_token(user_id, ip):
    import datetime
    # 过期时间为7天
    datetime_int = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
    option = {
        "iss": "rainbowinpaper.cn",  # token的签发者
        "exp": datetime_int,  # 过期时间
        "iat": datetime.datetime.utcnow(),
        "aud": "webkit",  # token的接收者，这里指定为浏览器
        "user_id": user_id,  # 放入用户信息，唯一标识，解析后可以使用该消息
        "ip": ip,  # token与ip绑定
        'type': 'access'
    }
    return jwt.encode(option, SECRET_KEY, "HS256")


def create_refresh_token(user_id, ip):
    import datetime
    # 过期时间为7天
    datetime_int = datetime.datetime.utcnow() + datetime.timedelta(days=7)
    option = {
        "iss": "rainbowinpaper.cn",  # token的签发者
        "exp": datetime_int,  # 过期时间
        "iat": datetime.datetime.utcnow(),
        "aud": "webkit",  # token的接收者，这里指定为浏览器
        "user_id": user_id,  # 放入用户信息，唯一标识，解析后可以使用该消息
        "ip": ip,  # token与ip绑定
        'type': 'refresh'
    }
    return jwt.encode(option, SECRET_KEY, "HS256")


def refresh_token(token):
    try:
        payload = jwt.decode(token, SECRET_KEY, audience="webkit", algorithms=["HS256"])
        if payload.get('type') != 'refresh':
            return None
        user_id = payload.get('user_id')
        ip = payload.get('ip')
        return {"token": create_token(user_id, ip), "refreshToken": jwt.encode(payload, SECRET_KEY, "HS256")}
    except jwt.ExpiredSignatureError:
        return None
    except jwt.DecodeError:
        return None
    except jwt.InvalidTokenError:
        return None


def check_token(token):
    try:
        data = jwt.decode(token, SECRET_KEY, audience="webkit", algorithms=["HS256"])
        if data.get('type') != 'access':
            return None
        return data
    except jwt.ExpiredSignatureError:
        return None
    except jwt.DecodeError:
        return None
    except jwt.InvalidTokenError:
        return None
