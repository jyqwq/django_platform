from auths import models
from django.db.models import Q


# 初始化权限管理菜单
def init_list():
    # 权限管理菜单
    auth = {
        "type": "catalog",
        "name": "权限管理",
        "path": "/auth",
        "rank": 1,
        "icon": "ApartmentOutlined",
        "pageType": "component",
        "link": "",
        "iframe": "",
        "permission": "auth",
        "target": "_self",
        "deleted": False,
        "renderMenu": True,
    }
    auth_info = models.AuthMenu.objects.create(**auth)
    if not isinstance(auth_info, dict):
        if auth_info.id:
            # 用户管理
            user = {
                "type": "menu",
                "name": "用户管理",
                "path": "user",
                "rank": 1,
                "icon": "TeamOutlined",
                "pageType": "component",
                "component": "Auth/user",
                "link": "",
                "iframe": "",
                "permission": "auth:user:list",
                "target": "_self",
                "deleted": False,
                "renderMenu": True,
                "parentId": auth_info.id,
            }
            # 角色管理
            role = {
                "type": "menu",
                "name": "角色管理",
                "path": "role",
                "rank": 2,
                "icon": "SolutionOutlined",
                "pageType": "component",
                "component": "Auth/role",
                "link": "",
                "iframe": "",
                "permission": "auth:user:role",
                "target": "_self",
                "deleted": False,
                "renderMenu": True,
                "parentId": auth_info.id,
            }
            # 菜单管理
            menu = {
                "type": "menu",
                "name": "菜单管理",
                "path": "menu",
                "rank": 3,
                "icon": "MenuOutlined",
                "pageType": "component",
                "component": "Auth/menu",
                "link": "",
                "iframe": "",
                "permission": "auth:user:menu",
                "target": "_self",
                "deleted": False,
                "renderMenu": True,
                "parentId": auth_info.id,
            }
            models.AuthMenu.objects.create(**user)
            models.AuthMenu.objects.create(**role)
            models.AuthMenu.objects.create(**menu)
            # 超级管理员
            admin = {
                "name": "超级管理员",
                "permission": "admin",
                "state": True,
                "deleted": False
            }
            admin_info = models.AuthRole.objects.create(**admin)
            user = {
                "name": "admin",
                "account": "admin",
                "phone": "",
                "password": "admin",
                "state": True,
                "deleted": False,
                "roleId": admin_info.id
            }
            user_info = models.AuthUser.objects.create(**user)
            if not isinstance(user_info, dict):
                if user_info.id:
                    return True
                else:
                    return False
        else:
            return False


def build_menu_tree(has_btn=True, items=None, parent_id=0):
    if items is None:
        items = []
    result = []
    for item in items:
        if item.get('type') == 'button' and not has_btn:
            continue
        new_item = {
            "name": item['name'],
            "path": item['path'],
            "component": item['component']
        }
        meta = {
            "title": item['name'],
            "icon": item['icon'],
            "pageType": item['pageType'],
            "link": item['link'],
            "iframe": item['iframe'],
            "permission": item['permission'],
            "target": item['target'],
            "type": item['type'],
            "renderMenu": item['renderMenu'],
            "order": item['rank'],
            "i18nTitle": "routes.dashboard._value",
            "requiresAuth": False
        }
        if item.get('parentId') is None:
            meta['parentId'] = 0
        else:
            meta['parentId'] = item['parentId']
        if meta.get('parentId') == parent_id:
            if meta.get('type') == 'catalog' or has_btn:
                children = build_menu_tree(has_btn, items, item['id'])
                if children:
                    new_item['children'] = children
            new_item['meta'] = meta
            result.append(new_item)
    return result


def get_permission_items(role_id):
    menu_ids = models.AuthPermission.objects.filter(roleId=role_id).values_list('menuId', flat=True)
    # 获取这些菜单及其关联父级菜单，按照rank字段排序
    menus = models.AuthMenu.objects.filter(Q(id__in=menu_ids) | Q(parentId__in=menu_ids))
    permission_items = []
    for menu in menus:
        if menu.permission != "":
            permission_items.append(menu.permission)
        parent_id = menu.parentId
        while parent_id != 0:
            parent_menu = models.AuthMenu.objects.get(id=parent_id)
            if parent_menu.permission != "":
                permission_items.append(parent_menu.permission)
            parent_id = parent_menu.parentId
    return list(set(permission_items))


def build_route_tree(use_mate, items=None, parent_id=0):
    if items is None:
        items = []
    result = []
    for item in items:
        permissions = item['permissions']
        if item.get('permissions') is not None and isinstance(item.get('permissions'), str) and item.get('permissions') != "":
            permissions = item['permissions'].split(",")
        if use_mate:
            new_item = {
                "name": item['name'],
                "path": item['path'],
                "component": item['component'],
                "redirect": item['redirect']
            }
            meta = {
                "title": item['title'],
                "dynamicPath": item['dynamicPath'],
                "i18nTitle": item['i18nTitle'],
                "href": item['href'],
                "icon": item['icon'],
                "localIcon": item['localIcon'],
                "hide": item['hide'],
                "multiTab": item['multiTab'],
                "order": item['order'],
                "activeMenu": item['activeMenu'],
                "requiresAuth": item['requiresAuth'],
                "permissions":  permissions,
                "keepAlive": item['keepAlive'],
                "createTime": item['createTime']
            }
            if item.get('parentId') is None:
                meta['parentId'] = 0
            else:
                meta['parentId'] = item['parentId']
            if meta.get('parentId') == parent_id:
                children = build_route_tree(use_mate, items, item['id'])
                if children:
                    new_item['children'] = children
                new_item['meta'] = meta
                result.append(new_item)
        else:
            item['permissions'] = permissions
            if item.get('parentId') is None:
                item['parentId'] = 0
            if item.get('parentId') == parent_id:
                children = build_route_tree(use_mate, items, item['id'])
                if children:
                    item['children'] = children
                result.append(item)
    return result
