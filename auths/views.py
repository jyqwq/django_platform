from django.http import HttpResponse, JsonResponse
import json
from auths import models
from utils import utils
from utils.token import check_token
from django.forms import model_to_dict


def index(request):
    return HttpResponse("Hello, world. You're at the auths index.")


def get_menu_list(request):
    try:
        if request.method == 'GET':
            menu_list = list(models.AuthMenu.objects.all().values())
            if not len(menu_list):
                init = utils.init_list()
                if init:
                    menu_list = list(models.AuthMenu.objects.all().values())
                else:
                    return JsonResponse({"state": 500, "message": "系统初始化错误", "data": None}, safe=False)
            tree_list = utils.build_menu_tree(True, menu_list)
            return JsonResponse({"state": 200, "message": "获取成功", "data": tree_list}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("get_menu_list error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def get_role_list(request):
    try:
        if request.method == 'GET':
            role_list = list(models.AuthRole.objects.all().values())
            for item in role_list:
                if item['permission'] == 'super':
                    item['permissionItems'] = ['*']
                else:
                    permission_items = list(models.AuthPermission.objects.filter(roleId=item['id']).values()) or []
                    for permission_item in permission_items:
                        menu = model_to_dict(models.AuthMenu.objects.get(id=permission_item['menuId']))
                        permission_item['permission'] = menu['permission']
                        permission_item['name'] = menu['name']
                    item['permissionItems'] = permission_items
            return JsonResponse({"state": 200, "message": "获取成功", "data": role_list}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("get_role_list error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def get_user_list(request):
    try:
        if request.method == 'GET':
            user_list = list(models.AuthUser.objects.all().values())
            for item in user_list:
                role = model_to_dict(models.AuthRole.objects.get(id=item['roleId']))
                item['roleName'] = role['name']
                item['permission'] = role['permission']
            return JsonResponse({"state": 200, "message": "获取成功", "data": user_list}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("get_user_list error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def edit_menu(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            if data.get('type') != 'button':
                if data.get('id') is None:
                    same_name = models.AuthMenu.objects.filter(name=data.get('name'), parentId=data.get('parentId') or 0)
                    same_path = models.AuthMenu.objects.filter(path=data.get('path'), parentId=data.get('parentId') or 0)
                else:
                    same_name = models.AuthMenu.objects.filter(name=data.get('name'), parentId=data.get('parentId') or 0)\
                        .exclude(id=data.get('id'))
                    same_path = models.AuthMenu.objects.filter(name=data.get('path'), parentId=data.get('parentId') or 0)\
                        .exclude(id=data.get('id'))
                if len(same_name) > 0:
                    return JsonResponse({"state": 200, "message": "同级菜单名称重复", "data": "name error"}, safe=False)
                if len(same_path) > 0:
                    return JsonResponse({"state": 200, "message": "同级菜单路径重复", "data": "path error"}, safe=False)
            if data.get('id') is None:
                menu = {
                    "type": data.get('type'),
                    "name": data.get('name'),
                    "path": data.get('path'),
                    "rank": data.get('rank') or 0,
                    "icon": data.get('icon') or '',
                    "pageType": data.get('pageType'),
                    "component": data.get('component'),
                    "link": data.get('link'),
                    "iframe": data.get('iframe'),
                    "permission": data.get('permission'),
                    "target": data.get('target'),
                    "renderMenu": data.get('renderMenu'),
                    "tabType": data.get('tabType'),
                    "parentId": data.get('parentId') or 0,
                    "deleted": True,
                }
                mid = models.AuthMenu(**menu)
                mid.save()
                return JsonResponse({"state": 200, "message": "添加成功", "data": mid.id}, safe=False)
            else:
                menu = {
                    "type": data.get('type'),
                    "name": data.get('name'),
                    "path": data.get('path'),
                    "rank": data.get('rank') or 0,
                    "icon": data.get('icon') or '',
                    "pageType": data.get('pageType'),
                    "component": data.get('component'),
                    "link": data.get('link'),
                    "iframe": data.get('iframe'),
                    "permission": data.get('permission'),
                    "target": data.get('target'),
                    "renderMenu": data.get('renderMenu'),
                    "tabType": data.get('tabType'),
                    "parentId": data.get('parentId') or 0,
                }
                models.AuthMenu.objects.filter(id=data.get('id')).update(**menu)
                return JsonResponse({"state": 200, "message": "更新成功", "data": data.get('id')}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("add_menu error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def delete_menu(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            if data.get('id'):
                event = models.AuthMenu.objects.get(id=data.get('id'))
                if model_to_dict(event)['deleted']:
                    event.delete()
                    return JsonResponse({"state": 200, "message": "删除成功", "data": data.get('id')}, safe=False)
                else:
                    return JsonResponse({"state": 200, "message": "不可删除", "data": data.get('id')}, safe=False)
            else:
                return JsonResponse({"state": 500, "message": "参数缺失", "data": None}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("delete_menu error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def edit_role(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            if data.get('id') is None:
                same_name = models.AuthRole.objects.filter(name=data.get('name'))
                same_permission = models.AuthRole.objects.filter(permission=data.get('permission'))
            else:
                same_name = models.AuthRole.objects.filter(name=data.get('name')).exclude(id=data.get('id'))
                same_permission = models.AuthRole.objects.filter(permission=data.get('permission')).exclude(id=data.get('id'))
            if len(same_name) > 0:
                return JsonResponse({"state": 200, "message": "角色名称重复", "data": "name error"}, safe=False)
            if len(same_permission) > 0:
                return JsonResponse({"state": 200, "message": "角色权限重复", "data": "permission error"}, safe=False)
            if data.get('id') is None:
                role = {
                    "name": data.get('name'),
                    "permission": data.get('permission'),
                    "state": data.get('state'),
                    "deleted": True,
                }
                rid = models.AuthRole(**role)
                rid.save()
                if rid.id:
                    objs = [models.AuthPermission(roleId=rid.id, menuId=item) for item in data.get('permissionIds')]
                    models.AuthPermission.objects.bulk_create(objs)
                    return JsonResponse({"state": 200, "message": "添加成功", "data": rid.id}, safe=False)
                else:
                    return JsonResponse({"state": 500, "message": "添加失败", "data": None}, safe=False)
            else:
                role = {
                    "name": data.get('name'),
                    "permission": data.get('permission'),
                    "state": data.get('state'),
                    "deleted": True,
                }
                models.AuthRole.objects.filter(id=data.get('id')).update(**role)
                per = models.AuthPermission.objects.filter(roleId=data.get('id'))
                if len(per) > 0:
                    per.delete()
                objs = [models.AuthPermission(roleId=data.get('id'), menuId=item) for item in data.get('permissionIds')]
                models.AuthPermission.objects.bulk_create(objs)
                return JsonResponse({"state": 200, "message": "更新成功", "data": data.get('id')}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("edit_role error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def delete_role(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            if data.get('id'):
                event = models.AuthRole.objects.get(id=data.get('id'))
                if model_to_dict(event)['deleted']:
                    event.delete()
                    per = models.AuthPermission.objects.filter(roleId=data.get('id'))
                    if len(per) > 0:
                        per.delete()
                    return JsonResponse({"state": 200, "message": "删除成功", "data": data.get('id')}, safe=False)
                else:
                    return JsonResponse({"state": 200, "message": "不可删除", "data": data.get('id')}, safe=False)
            else:
                return JsonResponse({"state": 500, "message": "参数缺失", "data": None}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("delete_role error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def edit_user(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            if data.get('id') is None:
                same_account = models.AuthUser.objects.filter(account=data.get('account'))
                same_phone = models.AuthUser.objects.filter(phone=data.get('phone'))
            else:
                same_account = models.AuthUser.objects.filter(account=data.get('account')).exclude(id=data.get('id'))
                same_phone = models.AuthUser.objects.filter(phone=data.get('phone')).exclude(id=data.get('id'))
            if len(same_account) > 0:
                return JsonResponse({"state": 200, "message": "账号重复", "data": "account error"}, safe=False)
            if len(same_phone) > 0:
                return JsonResponse({"state": 200, "message": "手机号重复", "data": "phone error"}, safe=False)
            if data.get('id') is None:
                user = {
                    "name": data.get('name'),
                    "account": data.get('account'),
                    "phone": data.get('phone'),
                    "password": data.get('password'),
                    "state": data.get('state'),
                    "deleted": True,
                    "roleId": data.get('roleId'),
                }
                uid = models.AuthUser(**user)
                uid.save()
                if uid.id:
                    return JsonResponse({"state": 200, "message": "添加成功", "data": uid.id}, safe=False)
                else:
                    return JsonResponse({"state": 500, "message": "添加失败", "data": None}, safe=False)
            else:
                user = {
                    "name": data.get('name'),
                    "account": data.get('account'),
                    "phone": data.get('phone'),
                    "password": data.get('password'),
                    "state": data.get('state'),
                    "roleId": data.get('roleId'),
                }
                models.AuthUser.objects.filter(id=data.get('id')).update(**user)
                return JsonResponse({"state": 200, "message": "更新成功", "data": data.get('id')}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("add_user error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def delete_user(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            if data.get('id'):
                event = models.AuthUser.objects.get(id=data.get('id'))
                if model_to_dict(event)['deleted']:
                    event.delete()
                    return JsonResponse({"state": 200, "message": "删除成功", "data": data.get('id')}, safe=False)
                else:
                    return JsonResponse({"state": 200, "message": "不可删除", "data": data.get('id')}, safe=False)
            else:
                return JsonResponse({"state": 500, "message": "参数缺失", "data": None}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("delete_user error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def get_auth_route_list(request):
    try:
        if request.method == 'POST':
            body = json.loads(request.body)
            if body.get("userId"):
                # 获取用户信息
                admin_user = model_to_dict(models.AuthUser.objects.get(id=body["userId"]))
                role = model_to_dict(models.AuthRole.objects.get(id=admin_user["roleId"]))
                # 超级管理员获取所有路由
                if role["permission"] == 'super':
                    route_list = list(models.AuthRoute.objects.all().values())
                    tree_list = utils.build_route_tree(True, route_list)
                    return JsonResponse({"state": 200, "message": "获取成功",
                                         "data": {"home": "dashboard_analysis", "routes": tree_list}}, safe=False)
                else:
                    # 通过角色id去角色路由映射表中获取路由id
                    per = models.AuthPermission.objects.filter(roleId=admin_user["roleId"])
                    route_list = list(models.AuthRoute.objects.filter(id__in=per.values_list('menuId', flat=True)).values())
                    # 通过路由id获取路由信息 构建路由树
                    tree_list = utils.build_route_tree(True, route_list)
                    return JsonResponse({"state": 200, "message": "获取成功",
                                         "data": {"home": "dashboard_analysis", "routes": tree_list}}, safe=False)
            else:
                return JsonResponse({"state": 500, "message": "参数错误", "data": {}}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("get_all_route_list error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def get_auth_role_list(request):
    try:
        if request.method == 'GET':
            if request.GET.get('id'):
                role = list(models.AuthPermission.objects.filter(menuId=request.GET.get('id')).values())
                return JsonResponse({"state": 200, "message": "获取成功", "data": role}, safe=False)
            else:
                return JsonResponse({"state": 500, "message": "参数错误", "data": {}}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("get_auth_role_list error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def get_all_route_list(request):
    try:
        if request.method == 'GET':
            route_list = list(models.AuthRoute.objects.all().values())
            tree_list = utils.build_route_tree(False, route_list)
            return JsonResponse({"state": 200, "message": "获取成功", "data": tree_list}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("get_all_route_list error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)


def edit_route(request):
    try:
        if request.method == 'POST':
            data = json.loads(request.body)
            if data.get('name') is None or data.get('path') is None:
                return JsonResponse({"state": 500, "message": "参数缺失", "data": None}, safe=False)
            route_id = data.get('id')
            parent_id = data.get('parentId') or 0
            if route_id is None:
                same_name = models.AuthRoute.objects.filter(name=data.get('name'), parentId=parent_id)
                same_path = models.AuthRoute.objects.filter(path=data.get('path'), parentId=parent_id)
            else:
                same_name = models.AuthRoute.objects.filter(name=data.get('name'), parentId=parent_id) \
                    .exclude(id=route_id)
                same_path = models.AuthRoute.objects.filter(name=data.get('path'), parentId=parent_id) \
                    .exclude(id=route_id)
            if len(same_name) > 0:
                return JsonResponse({"state": 500, "message": "同级路由名称重复", "data": None}, safe=False)
            if len(same_path) > 0:
                return JsonResponse({"state": 500, "message": "同级路由路径重复", "data": None}, safe=False)

            menu = {
                "parentId": parent_id,
                "name": data.get('name'),
                "title": data.get('title'),
                "path": data.get('path'),
                "dynamicPath": data.get('dynamicPath') or '',
                "redirect": data.get('redirect') or '',
                "component": data.get('component') or '',
                "i18nTitle": data.get('i18nTitle') or '',
                "href": data.get('href') or '',
                "icon": data.get('icon') or '',
                "localIcon": data.get('localIcon') or '',
                "hide": data.get('hide') or False,
                "multiTab": data.get('multiTab') or False,
                "order": data.get('order') or 0,
                "activeMenu": data.get('activeMenu') or '',
                "requiresAuth": data.get('requiresAuth') or True,
                "keepAlive": data.get('keepAlive') or False
            }
            if route_id is None:
                mid = models.AuthRoute(**menu)
                mid.save()
                objs = [models.AuthPermission(menuId=mid.id, roleId=item) for item in data.get('permissions')]
                models.AuthPermission.objects.bulk_create(objs)
                return JsonResponse({"state": 200, "message": "添加成功", "data": mid.id}, safe=False)
            else:
                models.AuthRoute.objects.filter(id=route_id).update(**menu)
                per = models.AuthPermission.objects.filter(menuId=route_id)
                if len(per) > 0:
                    per.delete()
                objs = [models.AuthPermission(roleId=item, menuId=route_id) for item in data.get('permissionIds')]
                models.AuthPermission.objects.bulk_create(objs)
                return JsonResponse({"state": 200, "message": "更新成功", "data": route_id}, safe=False)
        else:
            return JsonResponse({"state": 500, "message": "错误请求", "data": None}, safe=False)
    except Exception as ex:
        print("edit_route error")
        print(ex)
        return JsonResponse({"state": 500, "message": "服务器错误", "data": None}, safe=False)
