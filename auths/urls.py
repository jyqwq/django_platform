from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^getAuthRouteList', views.get_auth_route_list, name='get_auth_route_list'),
    url(r'^getAuthRoleList', views.get_auth_role_list, name='get_auth_role_list'),
    url(r'^getAllRouteList', views.get_all_route_list, name='get_all_route_list'),
    url(r'^getMenuList', views.get_menu_list, name='get_menu_list'),
    url(r'^getRoleList', views.get_role_list, name='get_role_list'),
    url(r'^getUserList', views.get_user_list, name='get_user_list'),
    url(r'^editRoute', views.edit_route, name='edit_route'),
    url(r'^editMenu', views.edit_menu, name='edit_menu'),
    url(r'^editRole', views.edit_role, name='edit_role'),
    url(r'^editUser', views.edit_user, name='edit_user'),
    url(r'^deleteMenu', views.delete_menu, name='delete_menu'),
    url(r'^deleteRole', views.delete_role, name='delete_role'),
    url(r'^deleteUser', views.delete_user, name='delete_user'),
]
