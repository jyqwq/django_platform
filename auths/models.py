from django.db import models


# 菜单表 已废弃
class AuthMenu(models.Model):
    objects = models.Manager()
    type = models.CharField(max_length=20)  # 类型 目录/菜单/按钮
    name = models.CharField(max_length=100)  # 名称
    path = models.CharField(max_length=100)  # 路径
    icon = models.CharField(max_length=100, blank=True, default="")  # 图标
    rank = models.IntegerField()  # 排序
    target = models.CharField(max_length=100)  # 打开方式
    link = models.CharField(max_length=255, blank=True, default="")  # 链接
    iframe = models.CharField(max_length=255, blank=True, default="")  # 内嵌链接
    component = models.CharField(max_length=255, blank=True, default="")  # 组件
    pageType = models.CharField(max_length=20)  # 页面类型
    renderMenu = models.BooleanField()  # 是否渲染菜单
    tabType = models.CharField(max_length=20, blank=True, default="")  # 页签类型
    permission = models.CharField(max_length=100, blank=True, default="")  # 权限标识
    parentId = models.IntegerField(default=0)  # 父级ID
    deleted = models.BooleanField()  # 是否删除
    createTime = models.DateTimeField(auto_now_add=True)  # 创建时间


# 角色表
class AuthRole(models.Model):
    objects = models.Manager()
    name = models.CharField(max_length=20, unique=True)  # 角色名称
    permission = models.CharField(max_length=255)  # 权限标识
    state = models.BooleanField()  # 状态
    deleted = models.BooleanField()  # 是否删除
    createTime = models.DateTimeField(auto_now_add=True)  # 创建时间


# 用户表
class AuthUser(models.Model):
    objects = models.Manager()
    name = models.CharField(max_length=8)  # 昵称
    account = models.CharField(max_length=20, unique=True)  # 账号
    phone = models.CharField(max_length=11)  # 手机号
    password = models.CharField(max_length=255)  # 密码
    state = models.BooleanField()  # 状态
    deleted = models.BooleanField()  # 是否删除
    roleId = models.IntegerField()  # 角色ID
    createTime = models.DateTimeField(auto_now_add=True)  # 创建时间


# 权限表
class AuthPermission(models.Model):
    objects = models.Manager()
    roleId = models.IntegerField()  # 角色ID
    menuId = models.IntegerField()  # 菜单ID


class AuthRoute(models.Model):
    objects = models.Manager()
    parentId = models.IntegerField(default=0)  # 父级ID
    name = models.CharField(max_length=100)  # 路由模块名
    title = models.CharField(max_length=100)  # 标题
    path = models.CharField(max_length=100)  # 路径
    dynamicPath = models.CharField(max_length=100, blank=True, default="")  # 动态路径
    redirect = models.CharField(max_length=100, blank=True, default="")  # 重定向
    component = models.CharField(max_length=100, blank=True, default="")  # 组件
    i18nTitle = models.CharField(max_length=100, blank=True, default="")  # 国际化
    href = models.CharField(max_length=255, blank=True, default="")  # 链接
    icon = models.CharField(max_length=100, blank=True, default="")  # 图标
    localIcon = models.CharField(max_length=100, blank=True, default="")  # 本地图标
    hide = models.BooleanField(default=False)  # 隐藏
    multiTab = models.BooleanField(default=False)  # 多页签
    order = models.IntegerField()  # 排序
    activeMenu = models.CharField(max_length=100, blank=True, default="")  # 激活菜单
    requiresAuth = models.BooleanField(default=True)  # 需要登录权限
    permissions = models.CharField(max_length=255, blank=True, default="")  # 权限标识
    keepAlive = models.BooleanField(default=False)  # 缓存页面
    createTime = models.DateTimeField(auto_now_add=True)  # 创建时间
