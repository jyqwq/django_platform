from django.contrib import admin

# Register your models here.
from .models import AuthUser, AuthRole, AuthMenu

admin.site.register(AuthUser)
admin.site.register(AuthRole)
admin.site.register(AuthMenu)
